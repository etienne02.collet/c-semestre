#pragma once

#include <iostream>

class Rectangle
{
public:
    // g++ -std=c++17 -Wall -W -Werror -g ./main.cpp ./rectangle.cpp -o c1-4-rectangle
    Rectangle(float length, float width);
    Rectangle(float length);
    Rectangle();
    /*
    Rectangle(float length, float width)
        : _length{length}, _width{width}
    {
    }
    */

    float get_length() const { return _length; }
    float get_width() const { return _width; }
    static void set_default_size(float val);

    void scale(float ratio);
    /*
    void scale(float ratio)
    {
        _length *= ratio;
        _width *= ratio;
    }
    */

private:
    float _length;
    float _width;
    static float _default_size;
};

std::ostream &operator<<(std::ostream &stream, const Rectangle &rec);