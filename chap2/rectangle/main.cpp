#include "rectangle.h"

#include <iostream>

int main()
{
    Rectangle rect(2.f, 4.f);
    std::cout << "{ L: " << rect.get_length() << ", W: " << rect.get_width() << " }" << std::endl;

    rect.scale(3);
    std::cout << "{ L: " << rect.get_length() << ", W: " << rect.get_width() << " }" << std::endl;

    Rectangle square(2.5f);
    std::cout << "{ L: " << square.get_length() << ", W: " << square.get_width() << " }" << std::endl;

    Rectangle::set_default_size(7.f);
    Rectangle s1;
    std::cout << s1;

    Rectangle::set_default_size(2.f);
    Rectangle s2;
    std::cout << s2;

    return 0;
}