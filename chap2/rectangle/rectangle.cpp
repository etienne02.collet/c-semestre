#include "rectangle.h"

Rectangle::Rectangle(float length, float width)
    : _length{length}, _width{width}
{
}

Rectangle::Rectangle(float length)
    : Rectangle{length, length}
{
}

Rectangle::Rectangle()
    : Rectangle{_default_size}
{
}

void Rectangle::scale(float ratio)
{
    _length *= ratio;
    _width *= ratio;
}

void Rectangle::set_default_size(float size)
{
    _default_size = size;
}

std::ostream &operator<<(std::ostream &stream, const Rectangle &rec)
{
    return stream << "{ L: " << rec.get_length() << ", W: " << rec.get_width() << " }" << std::endl;
}

float Rectangle::_default_size = 0.f;