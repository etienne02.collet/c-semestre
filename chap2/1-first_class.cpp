#include <string>
#include <iostream>

class Person
{
private:
    int _age = 0;
    std::string _name;

public:
    void set_name(const std::string &name)
    {
        _name = name;
    }

    void set_age(int age)
    {
        _age = age;
    }

    const std::string &get_name() const
    {
        return _name;
    }

    int get_age() const
    {
        return _age;
    }
};

int main()
{
    Person p;

    p.set_name("Batman");
    p.set_age(23);

    std::cout << "Person named '" << p.get_name() << "' is " << p.get_age() << " years old." << std::endl;

    return 0;
}