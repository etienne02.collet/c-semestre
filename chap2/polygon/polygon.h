#pragma once

#include <iostream>
#include <vector>

using Paire = std::pair<int, int>;

class Polygon
{
    // friend std::ostream &operator<<(std::ostream &stream, const Polygon &val);

public:
    void add_vertex(int a, int b)
    {
        _vectrice.emplace_back(a, b);
    }

    const Paire &get_vertex(unsigned long id) const;
    std::vector<Paire> _vectrice;
};

std::ostream &operator<<(std::ostream &stream, const Polygon &val);