#include "polygon.h"

std::ostream &operator<<(std::ostream &stream, const Polygon &val)
{
    for (const Paire &v : val._vectrice)
    {
        stream << "(" << v.first << "," << v.second << ") ";
    }

    return stream;
}

const Paire &Polygon::get_vertex(unsigned long id) const
{
    return _vectrice[id];
}