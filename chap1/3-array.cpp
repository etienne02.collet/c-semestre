#include <iostream>
#include <vector>

int main(int argc, char **argv)
{
    /*
    // int array[] = {0, 1, 2, 3};
    int size = 50;
    int arrayBis[size] = {};

    for (int i = 0; i < size; i++)
    {
        arrayBis[i] = i + 1;
    }

    // for (int i = 0; i < 4; ++i)
    // for (int value : array)
    for (int value : arrayBis)
    {
        std::cout << value << std::endl;
    }
    */
    if (argc != 2)
    {
        std::cerr << "there are error in command line";
        return -1;
    }

    int array_size = std::stoi(argv[1]);
    std::vector<int> array;

    for (int i = 0; i < array_size; ++i)
    {
        array.emplace_back(i + 1);
    }

    for (int value : array)
    {
        std::cout << value << std::endl;
    }
    return 0;
}