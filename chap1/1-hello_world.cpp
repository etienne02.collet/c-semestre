#include <iostream>
/*
La fonction main :
ligne pour compiler : g++ -std=c++17 -Wall -W -Werror -g ./1-hello_world.cpp -o c1-1-hello_world

echo $? pour voir le retour de l'execution

std::cout => sortie standard
std::in => entrée standard
|- > flux

flux << string
pour ajouter la chaine de caractere dans le flux
|- > possibiliter de mettre autre chose que des chaine de caractere
(std::cout << "I am " << 0 << "hot dog" <<<std::endl;)


endl => (end line) \n plus force flush

*/
int main()
{
    std::cout << "Helllo me!" << std::endl;
    return 0;
}
