#include <iostream>
#include <string>
#include <vector>

// Output result.
void display_result(int result)
{
    std::cout << "Result is " << result << std::endl;
}

int add(int start, std::vector<int> values)
{
    for (auto v : values)
    {
        start += v;
    }
    return start;
}

int sub(int start, std::vector<int> values)
{
    for (auto i = 1u; i < values.size(); ++i)
    {
        start -= values[i];
    }
    return start;
}

int mul(int start, std::vector<int> values)
{
    for (auto v : values)
    {
        start *= v;
    }
    return start;
}

int compute_result(char op, const std::vector<int> &values)
{
    switch (op)
    {
    case '+':
        return add(0, values);
    case '*':
        return mul(1, values);
    case '-':
        return sub(values[0], values);
    default:
        return 0;
    }
}

bool parse_params(char &ret_op, std::vector<int> &values, int argc, char **argv)
{
    if (argc < 2)
    {
        std::cerr << "Expected operator as first argument." << std::endl;
        return false;
    }

    std::string op = argv[1];

    if (op != "+" && op != "*" && op != "-")
    {
        std::cerr << "Expected operator to be '+', '*' or '-'." << std::endl;
        return false;
    }

    ret_op = op[0];

    for (auto arg_i = 2; arg_i < argc; ++arg_i)
    {
        auto value = std::stoi(argv[arg_i]);
        values.emplace_back(value);
    }

    if (ret_op == '-' && values.empty())
    {
        std::cerr << "Operator '-' expects at least one operand to substract from." << std::endl;
        return false;
    }
    return true;
}

int main(int argc, char **argv)
{
    char op = ' ';
    std::vector<int> values;
    // Parsing program parameters.
    if (!parse_params(op, values, argc, argv))
    {
        return -1;
    }

    // Process operation, depending on the operator.
    int result = compute_result(op, values);

    // Output result.
    display_result(result);

    return 0;
}
