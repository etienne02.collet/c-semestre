#include <memory>
#include <utility>
#include <iostream>

void display(const std::string &variable_name, const std::unique_ptr<int> &variable)
{
    std::cout << variable_name << " contains " << *variable << std::endl;
}

std::unique_ptr<int> passthrough(std::unique_ptr<int> val)
{
    std::unique_ptr<int> val = std::move(val);
    return val;
}

int main()
{
    std::unique_ptr<int> i1 = std::make_unique<int>(3);
    std::unique_ptr<int> i2 = std::make_unique<int>(4);
    display("i1", i1);
    display("i2", i2);
    return 0;
}