#include <iostream>
#include <memory>
#include <vector>

struct Test
{
    Test(int v)
        : value{v}
    {
        std::cout << "Constructor was called with " << v << "." << std::endl;
    }

    Test(const Test &other)
        : value{other.value}
    {
        std::cout << "Copy constructor was called." << std::endl;
    }

    int value = 0;
};

Test create_test(int value)
{
    Test result{value};
    return result;
}

Test create_test_double(int value)
{
    Test result = create_test(2 * value);
    return result;
}

/*
int main()
{
    Test test = create_test_double(3); // -> pas de copie non plus
    std::cout << test.value << std::endl;

    return 0;
}
*/
int main()
{
    std::vector<std::unique_ptr<int>> many_ints;

    auto value = std::make_unique<int>(3);

    if (value != nullptr)
    {
        std::cout << "value is " << *value << std::endl;
    }
    else
    {
        std::cout << "value is empty" << std::endl;
    }

    many_ints.emplace_back(std::move(value));

    if (value != nullptr)
    {
        std::cout << "value is " << *value << std::endl;
    }
    else
    {
        std::cout << "value is empty" << std::endl;
    }

    if (many_ints[0] != nullptr)
    {
        std::cout << "many_ints[0] is " << *many_ints[0] << std::endl;
    }
    else
    {
        std::cout << "many_ints[0] is empty" << std::endl;
    }

    return 0;
}