#include "cat.h"
#include <memory>
#include <iostream>

int main()
{
    std::unique_ptr<Cat> first = std::make_unique<Cat>("Loli");
    std::cout << *first << std::endl;

    std::unique_ptr<Cat> cat2;
    if (cat2 == nullptr)
    {
        std::cout << "cat2 is nullptr." << std::endl;
    }
    cat2 = std::make_unique<Cat>("Dartagnan");
    std::cout << *cat2 << std::endl;

    std::cout << "cat1 == cat2 ? " << (first == cat2) << std::endl;
    std::cout << "*cat1 == *cat2 ? " << (*first == *cat2) << std::endl;

    first.reset();
    if (first == nullptr)
    {
        std::cout << "first cat is nullptr." << std::endl;
    }

    return 0;
}